import argparse, sys, socket

# python check_gpio.py --site "SlowDownHome" --sensor
# "outdoor" --sensorsystem "28-000006152042" --server "192.168.1.10"

# nagios checks require OK,Warning,Critical and exit 0,1,2
# -w -c
# sudo modprobe w1-gpio
# sudo modprobe w1-therm

argparser = argparse.ArgumentParser(
    description='Arguments that may be parsed.', epilog="The Epilog")
argparser.add_argument('--site', type=str, required=True,
                       help='Site the Device is Located. Required.')
argparser.add_argument('--measure', type=str, default="C", help='C or F')
argparser.add_argument('--warning','-w', type=int,
                       help='Warning level, or lower value for Between')
argparser.add_argument('--critical', '-c', type=int,
                       help='Critical level, or greater value for Between')
argparser.add_argument('--warncritcompare', type=str, default="G",
                       help='G Greater or L Lesser or B Between -w and -c, If returned Value is greater than, warning or crit are triggered.')
argparser.add_argument('--sensor', type=str, required=True,
                       help='Sensor Real Name. i.e. "Outdoor"')
argparser.add_argument('--sensorsystem', type=str, required=True,
                       help='Sensor Real Name. i.e. "209710825416"')
argparser.add_argument('--sensortype', type=str, required=True,
                       help='Sensor Type(Temp,Humidity,Soil Temp,Soil Hydration,etc.): T,H,ST,SH')
argparser.add_argument('--syslogserver', type=str,
                       help='Server IP address or hostname')
argparser.add_argument('--syslogport', default=514, type=int,
                       help='Port number on the server')
argparser.add_argument('--redisserver', type=str,
                       help='Server IP address or hostname')
argparser.add_argument('--redisport', default=6379, type=int,
                       help='Port number on the server')
args = argparser.parse_args()

def SendSyslogMessage(SYSLOG_SERVER,SYSLOG_SERVER_PORT,SOURCEID,MESSAGE):
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM) # UDP
    msg = "<"+SOURCEID + "> " + MESSAGE
    sock.sendto(msg.encode('utf-8'), (SYSLOG_SERVER, SYSLOG_SERVER_PORT))

def GetSensorData(sensorsystem):
    sensordata = ""
    try:
        TemperatureReadFromSensor = open(
            "/sys/bus/w1/devices/" + sensorsystem + "/w1_slave")
    except Exception, e:
        print "Unable to open sensor File."
        print e
    else:
        #sensordata = ReturnC(TemperatureReadFromSensor.read())
        #print(str(args.measure).upper())
        if str(args.measure).upper() == "C":
            sensordata = ReturnC(TemperatureReadFromSensor.read())
        else:
            sensordata = ReturnFfromC(
                ReturnC(TemperatureReadFromSensor.read()))
        TemperatureReadFromSensor.close()
    finally:
        return sensordata

def ReturnC(text):
    secondline = text.split("\n")[1]
    temperaturedata = secondline.split(" ")[9]
    temperature = float(temperaturedata[2:])
    ctemp = temperature / 1000
    #print( "Celsius: {0}".format(str(ctemp)))
    return ctemp

def ReturnFfromC(ctemp):
    ftemp=ctemp*1.8+32
    #print("Fahrenheit: {0}".format(str(ftemp)))
    return ftemp

def GetNagiosStatus(warncritcompare,warning,critical,sensordata):
    nagiosexitstatus = 2 #default to critical
    nagiosoutput = "Critical: status not set."
    if warncritcompare.upper() == "G":
        #print("Greater")
        if sensordata >= critical:
            nagiosexitstatus = 2
            nagiosoutput = "Critical: %s is greater than %s"%(sensordata,critical)
        elif sensordata >= warning:
            nagiosexitstatus = 1
            nagiosoutput = "Warning: %s is greater than %s"%(sensordata,warning)
        elif sensordata < warning:
            nagiosexitstatus = 0
            nagiosoutput = "OK: %s is less than %s"%(sensordata,warning)
    elif warncritcompare.upper() == "L":
        #print("Less")
        if sensordata > warning:
            nagiosexitstatus = 0
            nagiosoutput = "OK: %s is greater than %s"%(sensordata,warning)
        elif sensordata <= warning:
            nagiosexitstatus = 1
            nagiosoutput = "Warning: %s is less than %s"%(sensordata,warning)
        elif sensordata <= critical:
            nagiosexitstatus = 2
            nagiosoutput = "Critical: %s is less than %s"%(sensordata,critical)
    elif warncritcompare.upper() == "B":
        #print("Between")
        if sensordata > critical or sensordata < warning:
            nagiosexitstatus = 2
            nagiosoutput = "Critical: %s is less than %s"%(sensordata,critical)
        else:
            nagiosexitstatus = 0
            nagiosoutput = "OK: %s is Between %s and %s"%(sensordata,warning,critical)
    return nagiosexitstatus,nagiosoutput

def main():
    sensordata = GetSensorData(args.sensorsystem)

    if args.redisserver:
        redisoutput = ""
    #nodeoutput = args.site + ";" + args.sensor + ";" + \
    #    args.sensorsystem + ";" + args.sensortype + "," + str(sensordata)
    #print(nodeoutput)

    nagiosexitstatus,nagiosoutput = GetNagiosStatus(args.warncritcompare,args.warning,args.critical,sensordata)
    print("{0} for {1} - {2}".format(nagiosoutput,args.site,args.sensor))
    if args.syslogserver:
        message = "{0} for {1} - {2}".format(nagiosoutput,args.site,args.sensor)
        SendSyslogMessage(args.syslogserver,args.syslogport,args.site+", "+args.sensor,message)
    sys.exit(nagiosexitstatus)


if __name__ == "__main__":
    main()
